package Project;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;


import bl.framework.api.SeleniumBase;
import Utils.ReadXL;

public class ProjectMethod extends SeleniumBase{
	@Parameters({"url","username","pass"})
	@BeforeMethod
	public void login(String url, String uname, String pwd) {
		startApp("chrome", url);
		clearAndType(locateElement("id", "username"), uname); 
		clearAndType(locateElement("id", "password"), pwd); 
		click(locateElement("class", "decorativeSubmit")); 
		click(locateElement("xpath", "(//div[@id='label']//a)[1]"));
	}
/*	@AfterMethod
	public void closeApp() {
		close();
	*/
	
	@DataProvider(name="XLdata")
public Object[][] inputdata() {
		return ReadXL.getDataXL();
	}

}

	