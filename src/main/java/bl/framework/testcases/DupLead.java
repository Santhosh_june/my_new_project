package bl.framework.testcases;

import org.testng.annotations.Test;

import Project.ProjectMethod;

public class DupLead extends ProjectMethod{
	@Test(groups="Smoke")
	public void DL() throws InterruptedException {
		click(locateElement("xpath", "//a[text()='Leads']"));
		click(locateElement("xpath", "//a[text()='Find Leads']"));
		click(locateElement("xpath", "//span[text()='Email']"));
		clearAndType(locateElement("xpath", "//input[@name='emailAddress']"), "santhoshkandasamy61@gmail.com");
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		Thread.sleep(1000);
		String text = getElementText(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-firstName']//a"));
		System.out.println(text);
		click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-firstName']//a"));
		click(locateElement("xpath", "//a[text()='Duplicate Lead']"));
		if(driver.getTitle().contains("Duplicate")) {
			System.out.println("PASS");
		}else {
			System.out.println("FAIL");
		}
		click(locateElement("xpath", "//input[@value='Create Lead']"));
		String text2 = getElementText(locateElement("xpath", "//span[@id='viewLead_firstName_sp']"));
		if(text.equals(text2)) {
			System.out.println("PASS");
		}
		else {
			System.out.println("FAIL");
		}
}
}