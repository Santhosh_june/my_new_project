package bl.framework.testcases;

import org.testng.annotations.Test;

import Project.ProjectMethod;

public class MergeLead extends ProjectMethod{
	@Test(groups="Sanity")
	public void ML() throws InterruptedException{
		click(locateElement("xpath", "//a[text()='Leads']"));
		click(locateElement("xpath", "//a[text()='Merge Leads']"));
		click(locateElement("xpath", "//table[@id='widget_ComboBox_partyIdFrom']/following-sibling::a"));
		switchToWindow(1);
		clearAndType(locateElement("xpath", "//label[text()='Lead ID:']/following::input"), "10014");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(1000);
		//WebDriverWait wait = new WebDriverWait(driver, 100);
		//wait.until(ExpectedConditions.visibilityOf(locateElement("xpath", "//div[@class='x-grid3-row   ']//table//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a")));
		click(locateElement("xpath", "//div[@class='x-grid3-row   ']//table//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a"));
		switchToWindow(0);
		click(locateElement("xpath", "//table[@name='ComboBox_partyIdTo']/following-sibling::a"));
		switchToWindow(1);
		clearAndType(locateElement("xpath", "//label[text()='Lead ID:']/following::input"), "10015");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(1000);
		click(locateElement("xpath", "//div[@class='x-grid3-row   ']//table//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a"));
		switchToWindow(0);
		click(locateElement("xpath", "//a[text()='Merge']"));
		switchToAlert();
		acceptAlert();
		click(locateElement("xpath","//a[text()='Find Leads']"));
		clearAndType(locateElement("xpath", "(//div[@class='x-form-element']//input[@class=' x-form-text x-form-field'])[14]"), "10014");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		System.out.println(locateElement("xpath", "//div[text()='No records to display']").getText());

}
}