package bl.framework.testcases;
import org.testng.annotations.Test;

import Project.ProjectMethod;

public class EditLead extends ProjectMethod{
	/*(dependsOnMethods= {"bl.framework.testcases.CreateLead.CL"},invocationCount=2, invocationTimeOut=30000)*//*(groups="Sanity")*/
	
	
	@Test
	public void EL() throws InterruptedException {
		//login();
		click(locateElement("xpath", "//a[text()='Leads']"));
		click(locateElement("xpath", "//a[text()='Find Leads']"));
		clearAndType(locateElement("xpath", "(//input[@name='firstName'])[3]"), "Santhosh");
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		//String text = getElementText(locateElement("xpath", "(//div[@class='x-grid3-scroller']//a)[1]"));
		Thread.sleep(1000);
		click(locateElement("xpath", "(//div[@class='x-grid3-scroller']//a)[1]"));
		if(driver.getTitle().equals("View Lead | opentaps CRM")) {
			System.out.println("Title validation Pass");
		}
		else {
			System.out.println("Title validation Fail");
		}
		click(locateElement("xpath", "//a[text()='Edit']"));
		clearAndType(locateElement("xpath", "(//div[@class='fieldgroup-body'])//input[@name='companyName']"), "CTS");
		String string = locateElement("xpath", "(//div[@class='fieldgroup-body'])//input[@name='companyName']").getAttribute("value");
		click(locateElement("xpath", "//input[@value='Update']"));
		verifyPartialText(locateElement("xpath", "//span[@id='viewLead_companyName_sp']"), string);
		//		close();
	}
	

}