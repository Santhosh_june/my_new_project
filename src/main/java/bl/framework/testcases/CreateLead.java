package bl.framework.testcases;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import Project.ProjectMethod;
//import bl.framework.api.SeleniumBase;

public class CreateLead extends ProjectMethod{
	@Test/*(groups="Smoke")*/(dataProvider="XLdata")
	public void CL(Object cname, Object name , Object sec ) {
		//login(); --> Annotation set to beforemethod in ProjectMethod class in login method
		click(locateElement("xpath", "//a[text()='Create Lead']"));
		clearAndType(locateElement("id", "createLeadForm_companyName"), ""+cname);
		clearAndType(locateElement("id", "createLeadForm_firstName"), ""+name);
		String attribute = locateElement("id", "createLeadForm_firstName").getAttribute("value");
		clearAndType(locateElement("id", "createLeadForm_lastName"), ""+sec);
		/*clearAndType(locateElement("id", "createLeadForm_primaryEmail"), "santhoshkandasamy61@gmail.com");
		selectDropDownUsingText(locateElement("id", "createLeadForm_dataSourceId"), "Self Generated");
		selectDropDownUsingIndex(locateElement("id", "createLeadForm_ownershipEnumId"), 1);*/
		click(locateElement("xpath", "//input[@value='Create Lead']"));
		String text = locateElement("xpath", "//span[@id='viewLead_firstName_sp']").getText();
		if(attribute.equals(text)) {
			System.out.println("PASS");
		}
		else {
			System.out.println("FAIL");
		}
		close();
	}
	
	
/*	@DataProvider(name="Data1")
	public String[][] fetchData() {
		String[][] data = new String[2][3];
		data[0][0]="TestLeaf";
		data[0][1]="Hulk";
		data[0][2]="C";
		
		data[1][0]="TestLeaf";
		data[1][1]="Ram";
		data[1][2]="J";
		return data;
	}
	
	@DataProvider(name="Data2")
	public String[][] fetchData1() {
		String[][] data = new String[2][3];
		data[0][0]="TestLeaf";
		data[0][1]="kumar";
		data[0][2]="C";
		
		data[1][0]="TestLeaf";
		data[1][1]="pichu";
		data[1][2]="J";
		return data;
	}*/
}
