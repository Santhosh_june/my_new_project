package bl.framework.testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class TC001_LoginAndLogout extends SeleniumBase{
@Parameters({"url","username","pass"})
	@BeforeMethod
	public void login(String url, String uname, String pwd) {
		startApp("chrome", url);
		clearAndType(locateElement("id", "username"), uname); 
		clearAndType(locateElement("id", "password"), pwd); 
		click(locateElement("class", "decorativeSubmit")); 
		click(locateElement("xpath", "(//div[@id='label']//a)[1]"));
	}
	
	/*public void Duplead() throws InterruptedException {
		startApp("chrome", "http://leaftaps.com/opentaps");
		clearAndType(locateElement("id", "username"), "DemoSalesManager"); 
		clearAndType(locateElement("id", "password"), "crmsfa"); 
		click(locateElement("class", "decorativeSubmit")); 
		click(locateElement("xpath", "(//div[@id='label']//a)[1]"));
		click(locateElement("xpath", "//a[text()='Leads']"));
		click(locateElement("xpath", "//a[text()='Find Leads']"));
		click(locateElement("xpath", "//span[text()='Email']"));
		clearAndType(locateElement("xpath", "//input[@name='emailAddress']"), "santhoshkandasamy61@gmail.com");
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		Thread.sleep(1000);
		String text = getElementText(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-firstName']//a"));
		System.out.println(text);
		click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-firstName']//a"));
		click(locateElement("xpath", "//a[text()='Duplicate Lead']"));
		if(driver.getTitle().contains("Duplicate")) {
			System.out.println("PASS");
		}else {
			System.out.println("FAIL");
		}
		click(locateElement("xpath", "//input[@value='Create Lead']"));
		String text2 = getElementText(locateElement("xpath", "//span[@id='viewLead_firstName_sp']"));
		if(text.equals(text2)) {
			System.out.println("PASS");
		}
		else {
			System.out.println("FAIL");
		}
		close();
	}*/
	
	/*public void Editlead() throws InterruptedException {
		startApp("chrome", "http://leaftaps.com/opentaps");
		clearAndType(locateElement("id", "username"), "DemoSalesManager"); 
		clearAndType(locateElement("id", "password"), "crmsfa"); 
		click(locateElement("class", "decorativeSubmit")); 
		click(locateElement("xpath", "(//div[@id='label']//a)[1]"));
		click(locateElement("xpath", "//a[text()='Leads']"));
		click(locateElement("xpath", "//a[text()='Find Leads']"));
		clearAndType(locateElement("xpath", "(//input[@name='firstName'])[3]"), "Santhosh");
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		//String text = getElementText(locateElement("xpath", "(//div[@class='x-grid3-scroller']//a)[1]"));
		Thread.sleep(1000);
		click(locateElement("xpath", "(//div[@class='x-grid3-scroller']//a)[1]"));
		if(driver.getTitle().equals("View Lead | opentaps CRM")) {
			System.out.println("Title validation Pass");
		}
		else {
			System.out.println("Title validation Fail");
		}
		click(locateElement("xpath", "//a[text()='Edit']"));
		clearAndType(locateElement("xpath", "(//div[@class='fieldgroup-body'])//input[@name='companyName']"), "CTS");
		String string = locateElement("xpath", "(//div[@class='fieldgroup-body'])//input[@name='companyName']").getAttribute("value");
		click(locateElement("xpath", "//input[@value='Update']"));
		verifyPartialText(locateElement("xpath", "//span[@id='viewLead_companyName_sp']"), string);
		close();
	}*/
	
/*	public void Mergelead() throws InterruptedException{
		startApp("chrome", "http://leaftaps.com/opentaps");
		clearAndType(locateElement("id", "username"), "DemoSalesManager"); 
		clearAndType(locateElement("id", "password"), "crmsfa"); 
		click(locateElement("class", "decorativeSubmit")); 
		click(locateElement("xpath", "(//div[@id='label']//a)[1]"));
		click(locateElement("xpath", "//a[text()='Leads']"));
		click(locateElement("xpath", "//a[text()='Merge Leads']"));
		click(locateElement("xpath", "//table[@id='widget_ComboBox_partyIdFrom']/following-sibling::a"));
		switchToWindow(1);
		clearAndType(locateElement("xpath", "//label[text()='Lead ID:']/following::input"), "10014");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(1000);
		//WebDriverWait wait = new WebDriverWait(driver, 100);
		//wait.until(ExpectedConditions.visibilityOf(locateElement("xpath", "//div[@class='x-grid3-row   ']//table//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a")));
		click(locateElement("xpath", "//div[@class='x-grid3-row   ']//table//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a"));
		switchToWindow(0);
		click(locateElement("xpath", "//table[@name='ComboBox_partyIdTo']/following-sibling::a"));
		switchToWindow(1);
		clearAndType(locateElement("xpath", "//label[text()='Lead ID:']/following::input"), "10015");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(1000);
		click(locateElement("xpath", "//div[@class='x-grid3-row   ']//table//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a"));
		switchToWindow(0);
		click(locateElement("xpath", "//a[text()='Merge']"));
		switchToAlert();
		acceptAlert();
		click(locateElement("xpath","//a[text()='Find Leads']"));
		clearAndType(locateElement("xpath", "(//div[@class='x-form-element']//input[@class=' x-form-text x-form-field'])[14]"), "10014");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		System.out.println(locateElement("xpath", "//div[text()='No records to display']").getText());
		
	}*/

}








