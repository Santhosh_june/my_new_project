package bl.framework.api;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import bl.framework.base.Browser;
import bl.framework.base.Element;
import cucumber.api.java.lu.a;

public class SeleniumBase implements Element, Browser {

	public RemoteWebDriver driver;
	public int i =1;
	@Override
	public void startApp(String url) {
		// TODO Auto-generated method stub

	}

	@Override
	public void startApp(String browser, String url) {
		ChromeOptions op = new ChromeOptions();
		op.addArguments("--disable-notifications");
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver(op);
		} else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver = new FirefoxDriver(); 
		}
		//driver = new ChromeDriver(op);
		driver.manage().window().maximize();
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The browser "+browser+" launched successfully");
        takeSnap();
	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		switch (locatorType) {
		case "id": return driver.findElementById(value);
		case "name": return driver.findElementByName(value);
		case "class": return driver.findElementByClassName(value);
		case "xpath": return driver.findElementByXPath(value);
		default:
			break;
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		switch (type) {
		case "id": return driver.findElementById(value).findElements(By.tagName("tr"));
		case "name": return driver.findElementByName(value).findElements(By.tagName("tr"));
		case "class": return driver.findElementByClassName(value).findElements(By.tagName("tr"));
		case "xpath": return driver.findElementByXPath(value).findElements(By.tagName("tr"));
		default:
			break;
		}
		
		return null;
	}

	@Override
	public void switchToAlert() {
		try {
			driver.switchTo().alert();
			System.out.println("Successfully switched to Alert");
		} catch (NoAlertPresentException e) {
			System.out.println("No Alert popedup");
			//e.printStackTrace();
		}

	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
			System.out.println("Alert acceptted");
		} catch (NoAlertPresentException e) {
			System.out.println("No Alert popedup");
			//e.printStackTrace();
		}

	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			System.out.println("Alert Dismissed");
		} catch (NoAlertPresentException e) {
			System.out.println("No Alert popedup");
			//e.printStackTrace();
		}

	}

	@Override
	public String getAlertText() {
		try {
			String Altext = driver.switchTo().alert().getText();
			System.out.println("Alert text capttured successfully");
			return Altext;
		} catch (NoAlertPresentException e) {
			System.out.println("No Alert popedup");
			//e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public void typeAlert(String data) {
		try {
			driver.switchTo().alert().sendKeys(data);
			System.out.println("Text entered successfully in Alert textbox");
		} catch (NoAlertPresentException e) {
			System.out.println("No Alert popedup");
			//e.printStackTrace();
		}

	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> windowHandles = driver.getWindowHandles();
			List<String> ls = new ArrayList<>();
			ls.addAll(windowHandles);
			ls.get(1);
			driver.getTitle();
			driver.switchTo().window(ls.get(index));
			System.out.println("Successfully swiched to "+ls.get(index));
			//takeSnap();
		} catch (NoSuchWindowException e) {
			System.out.println("No window available for the given index");
			//e.printStackTrace();
		}

	}

	@Override
	public void switchToWindow(String title) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToFrame(int index) {
		try {
			driver.switchTo().frame(index);
			System.out.println("Successfully Switch to the frame using given index ");
		} catch (NoSuchFrameException e) {
			System.out.println("No able to identify the frame using given index");
			//e.printStackTrace();
		}

	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			System.out.println("Successfully Switch to the frame using given Webelement ");
		} catch (NoSuchFrameException e) {
			System.out.println("No able to identify the frame using given Webelwment");
			//e.printStackTrace();
		}
		catch(StaleElementReferenceException e){
			System.out.println("The given WebElement is no longer exist");
		}

	}

	@Override
	public void switchToFrame(String idOrName) {
		try {
			driver.switchTo().frame(idOrName);
			System.out.println("Successfully Switch to the frame using given idOrName ");
		} catch (NoSuchFrameException e) {
			System.out.println("No able to identify the frame using given idOrName");
			//e.printStackTrace();
		}

	}

	@Override
	public void defaultContent() {

		driver.switchTo().defaultContent();
		System.out.println("Control switched to first window/frame");

	}

	@Override
	public boolean verifyUrl(String url) {
		if(url.equals(driver.getCurrentUrl())) {
			System.out.println("URL verification pass");
			return true;
		}
		else {
			System.out.println("URL verification Fail");
			return false;
		}
		
	}

	@Override
	public boolean verifyTitle(String title) {
		if(title.equals(driver.getTitle())) {
			System.out.println("Title "+title+" verification pass");
			return true;
		}
		else {
			System.out.println("Title "+title+" verification Fail");
			return false;
		}
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void close() 
	{
		driver.close();
		System.out.println("Browser closed successfully");

	}

	@Override
	public void quit() 
	{
		driver.quit();
		System.out.println("All the browsers closed successfully");

	}
	

	@Override
	public void click(WebElement ele)
	{String text = ele.getText();
		try {
				ele.click();
				System.out.println("The element "+text+" clicked successfully");
				takeSnap();
			} 	
				catch (StaleElementReferenceException e) 
				{
					System.out.println("The WebElement has becomes Stale at this point");
				}
	}
	

	@Override
	public void append(WebElement ele, String data) 
	{
		try {
				ele.sendKeys(data);
				System.out.println("The given text "+data+" entered successfully");
				takeSnap();
			} 
				catch (ElementNotInteractableException e) 
				{
					System.out.println("Not able to enter the given date "+data+" in the idenified textfield ");
				}
					catch(IllegalArgumentException e) 
					{
						System.out.println("The provided data is NULL");
					}
	}
	

	@Override
	public void clear(WebElement ele) 
	{
		try {
			ele.clear();
			System.out.println("Value has been cleared successfully");
			takeSnap();
			} 
				catch (InvalidElementStateException e) 
				{
					System.out.println("The identified WebElemen is not Enabled/not a editibal Textfiel");
				}	
	}

	@Override
	public void clearAndType(WebElement ele, String data) 
	{
		try {
			ele.clear();
			ele.sendKeys(data); 
			System.out.println("The data "+data+" entered successfully");
			takeSnap();
			} 
				catch (ElementNotInteractableException e) 
				{
					System.out.println("Not able to enter the given date "+data+" in the idenified textfield ");
				}
					catch(IllegalArgumentException e) 
					{
						System.out.println("The provided data is NULL");
					}
	}
	

	@Override
	public String getElementText(WebElement ele) 
	{
		return ele.getText();
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) 
	{
		try {
			Select sel = new Select(ele);
			sel.selectByVisibleText(value);
			System.out.println("The Expected value "+value+" seleced successfully");
			takeSnap();
			} 
				catch (NoSuchElementException e)
				{
					System.out.println("Not able to identify the dropdown/Text");
				} 
	}

	
	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) 
	{
		try {
			Select sel = new Select(ele);
			sel.selectByIndex(index);
			System.out.println("The Expected value is chosed successfully from the given Index "+index);
			takeSnap();
			} 
			catch (NoSuchElementException e) 
				{
					System.out.println("Not able to identify the dropdown/Text");
				} 
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) 
	{
		try {
			Select sel = new Select(ele);
			sel.selectByValue(value);
			System.out.println("The Expected value "+value+" chosed successfully");
			takeSnap();
			} 
			catch (NoSuchElementException e) 
			{
				System.out.println("Not able to identify the dropdown or the value in it");
			} 
		
	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if(text.equals(expectedText)) {
			System.out.println("ExactText verification PASS");
			return true;
		}else {
			System.out.println("ExactText verification FAIL");
			return false;
		}
		
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if(text.contains(expectedText)) {
			System.out.println("PartialText verification PASS");
			return true;
		}else {
			System.out.println("PartialText verification FAIL");
			return false;
		}
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		if(ele.getAttribute(attribute).equals(value)) {
			System.out.println(value+" verification PASS");
			return true;
		}else {
			System.out.println(value+" verification FAIL");
			return false;
		}
	}

	@Override
	public boolean verifyPartialAttribute(WebElement ele, String attribute, String value) {
		if(ele.getAttribute(attribute).contains(value)) {
			System.out.println(value+" verification PASS");
			return true;
		}else {
			System.out.println(value+" verification FAIL");
			return false;
		}
		
	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		if(ele.isDisplayed()) {
			System.out.println("WebElement is displayed in DOM");
			return true;
		}else {
			System.out.println("WebElement is not displayed in DOM");
			return false;
		}
		
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		if(ele.isEnabled()) {
			System.out.println("The WebElement is Enabled");
			return true;
			//takeSnap();
		}
		else {
			System.out.println("The WebElement is Not Enabled");
			return false;
			//takeSnap();
		}
		
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		if(ele.isSelected()) {
			System.out.println("The WebElement is Selected");
			return true;
			//takeSnap();
		}
		else {
			System.out.println("The WebElement is Not Selected");
			return false;
			//takeSnap();
		}
	}

}
