package ProjectDay;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;

import bl.framework.api.SeleniumBase;


public class ZoomCar extends SeleniumBase{

	public static void main(String[] args) {
		ZoomCar PD = new ZoomCar();
		PD.ZC();
	}
	
	public void ZC() {
		startApp("chrome", "https://www.zoomcar.com/chennai");
		click(locateElement("xpath", "//a[text()='Start your wonderful journey']"));
		click(locateElement("xpath", "//div[@class='component-popular-locations']//div[text()='Popular Pick-up points']/following-sibling::div"));
		click(locateElement("xpath", "//div[@class='actions']//button"));
		String text = getElementText(locateElement("xpath", "(//div[@class='days']//div)[3]"));
		String replaceAll = text.replaceAll("\\D", "");
		System.out.println(replaceAll);
		click(locateElement("xpath", "(//div[@class='days']//div)[3]"));
		click(locateElement("xpath", "//button[text()='Next']"));
		String text2 = locateElement("xpath", "//div[@class='label time-label']").getText();
		String[] split = text2.split(" ");
		if(replaceAll.equals(split[1])) {
			System.out.println("Date verification PASS");
		}
		else{
			System.out.println("Date verification Fail");
		}
		
		click(locateElement("xpath", "//button[text()='Done']"));
		List<WebElement> Car = driver.findElementsByXPath("//div[@class='price']");
		List<Integer> pri = new ArrayList<>();
		for (WebElement webElement : Car) {
			String text4 = webElement.getText();
			String replaceAll1 = text4.replaceAll("\\D", "");
			int parseInt = Integer.parseInt(replaceAll1);
			pri.add(parseInt);
		}
	Collections.sort(pri);
	Integer integer = pri.get(pri.size()-1);
	String text3 = locateElement("xpath", "//div[contains(text(),'"+integer+"')]/parent::div/parent::div/preceding-sibling::div[@class='details']//h3").getText();
	System.out.println(text3);
	click(locateElement("xpath", "(//div[contains(text(),'"+integer+"')]/following-sibling::div)/following-sibling::button"));
	close();
	}
}
