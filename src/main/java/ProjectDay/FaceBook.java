package ProjectDay;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class FaceBook extends SeleniumBase{
	WebElement webElement12;
	
	@Test
	public void FB() throws InterruptedException {
		startApp("chrome", "https://en-gb.facebook.com/login/");
		clearAndType(locateElement("xpath", "//input[@id='email']"), "8122677553");
		clearAndType(locateElement("xpath", "//input[@id='pass']"), "sandy01june06");
		click(locateElement("xpath", "//button[@id='loginbutton']"));
		clearAndType(locateElement("xpath", "//input[@class='_1frb']"), "Testleaf");
		click(locateElement("xpath", "//button[@data-testid='facebar_search_button']"));
		List<WebElement> xPath = driver.findElementsByXPath("//div[@style='-webkit-line-clamp: 2;']");
		for (WebElement webElement1 : xPath) {
			//String str = webElement1; 
			String text = webElement1.getText();
			if(text.equalsIgnoreCase("Testleaf")) {
			 //webElement12 = webElement1;
				
				Thread.sleep(1000);
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.elementToBeClickable(locateElement("xpath", "//div[@style='-webkit-line-clamp: 2;']/../following-sibling::div//button")));
				String string = locateElement("xpath", "//div[@style='-webkit-line-clamp: 2;']/../following-sibling::div//button").getText();
				System.out.println("button text is "+string);
				
				String text2 = getElementText(locateElement("xpath", "//span[contains(@id,'u_ps_fetchstream')]//span"));
				System.out.println(text2);
				
			}
		}
		
		
	}
}
