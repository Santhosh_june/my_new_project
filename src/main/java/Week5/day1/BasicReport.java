package Week5.day1;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReporter;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


public class BasicReport {
	
	ExtentHtmlReporter html;
	ExtentReports extent;
	ExtentTest test;
	@Test
	public void Reportest() {
		
		html = new ExtentHtmlReporter("./report/Extend.html");
		extent = new ExtentReports();
		// see the report for all the  cycle
		html.setAppendExisting(true);
		extent.attachReporter(html);
		test = extent.createTest("TC001", "LOGIN TF");
		test.assignAuthor("Santhosh");
		test.assignCategory("System Testing");
		
		try {
			test.fail("Tried successfully",
			MediaEntityBuilder.createScreenCaptureFromPath("./../Snaps/img1.png").build());
		} catch (IOException e) {
			e.printStackTrace();
		}
		extent.flush();
		
	}
	

}
